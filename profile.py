"""This profile instantiates the L3H PL2 project firewall/nat with L3H server appliance.  It includes a NAT proxy node that connects to the internal (experimental net) LAN that the devices communicate on.

Instructions:
Swap in.
"""

import geni.portal as portal
import geni.rspec.pg as pg
import geni.rspec.emulab as elab
import geni.rspec.emulab.spectrum as spectrum

PROXY_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"
PROXY_SCRIPT = "/local/repository/bin/proxy_start.sh"

L3HMEBSERVER = "l3hserver1-meb3555"
PROXYGWADDR = "192.168.1.1"
SHVLANNAME = "L3HSHVLAN"
SHVLANMASK = "255.255.255.0"

# Parameters
pc = portal.Context()

pc.defineParameter("proxy_hwtype", "Proxy Node Type",
                   portal.ParameterType.STRING, "d710",
                   longDescription="Hardware type for compute node that will act as the proxy. Leave blank for 'any'.")

# Set of additional nodes/servers to attach behind proxy
portal.context.defineStructParameter(
    "servers", "Additional server nodes", [],
    multiValue=True,
    min=0,
    multiValueTitle="Additional server nodes",
    members=[
        portal.Parameter(
            "node_id",
            "Server Node ID",
            portal.ParameterType.STRING, L3HMEBSERVER,
            longDescription="Node ID of server to add to proxied LAN."
        ),
    ])

# pc.defineParameter("l3hserver_name","Node ID of L3H server",
#                    portal.ParameterType.STRING,L3HMEBSERVER,
#                    longDescription="This is the name of blackbox node setup in the DB for the L3H server.")

pc.defineParameter("gwaddr", "Gateway address for proxy on shared vlan",
                   portal.ParameterType.STRING, PROXYGWADDR,
                   advanced=True)

pc.defineParameter("sharedVlanName","Shared VLAN Name",
                   portal.ParameterType.STRING,SHVLANNAME,
                   longDescription="This is the name of the shared VLAN that the separate AP experiments will attach to. YOU MUST SET THIS TO SOMETHING VALID!",
                   advanced=True)

pc.defineParameter("sharedVlanNetmask", SHVLANMASK,
                   portal.ParameterType.STRING,"255.255.255.0",
                   advanced=True)

pc.defineParameter("createSharedVlan", "Create Shared VLAN",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Create the shared VLAN. (Disable this if the VLAN already exists.)",
                   advanced=True)

pc.defineParameter("tagSharedVlan", "Use Tagged Interfaces",
                   portal.ParameterType.BOOLEAN, False,
                   longDescription="Use tagged interfaces on nodes connected to the shared vlan.",
                   advanced=True)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

# Create the "inner egress LAN"
lan1 = request.LAN("lan1")
lan1.vlan_tagging = params.tagSharedVlan
lan1.setNoBandwidthShaping()
if params.createSharedVlan:
    lan1.createSharedVlan(params.sharedVlanName)
else:
    lan1.connectSharedVlan(params.sharedVlanName)

# Request the NAT proxy node.
proxy = request.RawPC("proxy")
proxy.hardware_type = params.proxy_hwtype
proxy.disk_image = PROXY_IMG
proxy.addService(pg.Execute(shell="sh", command=PROXY_SCRIPT))
pxlif = proxy.addInterface("pxlif", pg.IPv4Address(params.gwaddr, params.sharedVlanNetmask))
lan1.addInterface(pxlif)

# Request device representing L3H gear
for sname in params.servers:
    node = request.RawPC(sname.node_id)
    node.component_id = sname.node_id
    lan1.addNode(node)

# Dummy freq spec (needed for irisclients2-meb)
#request.requestSpectrum(3560, 3570, 0)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec()
